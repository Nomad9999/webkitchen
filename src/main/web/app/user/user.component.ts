import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserService } from './user.service';
import { UserAccount } from './user-account';

interface MessageJson {
    title: string;
    message: string;
}

@Component({
    selector: 'user',
    templateUrl: 'app/user/user.component.html',
    providers: [UserService]
})
export class UserComponent implements OnInit, OnDestroy {

    // vars
    private jsonResponse: string;
    private messages: Array<MessageJson>;
    private _subscription;
    private users: Array<UserAccount>;

    // constructor
    constructor(private _userService: UserService) {}

    // on-init
    ngOnInit() {
        // save _subscription
        this._subscription = this._userService.getTest()
            .subscribe(
                (data) => {
                    this.jsonResponse = JSON.stringify(data);
                    this.messages = data;
                    this.users = data;
                },
                (err) => {},
                () => {}
        );
    }

    // on-destroy
    ngOnDestroy() {
        // unsubscribe
        this._subscription.unsubscribe();
    }
}