export class Messages{
    static NOTIF_USER_WILL_OF_PART = ' wyraził chęć uczestnictwa w wydarzeniu: ';
    static NOTIF_PART_ACCEPTED = 'Zaakceptowano twoje uczestnictwo w wydarzeniu: ';
    static MESSAGE_PART_ACCEPTED = 'Akceptacja uczestnictwa - ';
    static MESSAGE_PART_DECLINED = 'Odrzucenie uczestnictwa - ';
    static NOTIF_PART_DECLINED = 'Odrzucono twoje uczestnictwo w wydarzeniu: ';
}