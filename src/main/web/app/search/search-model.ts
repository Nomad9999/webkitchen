export interface SearchModel{
    title: string
    address: string;
    date: any;
    typeEvent: string;
}