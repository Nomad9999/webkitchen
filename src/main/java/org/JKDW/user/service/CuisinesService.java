package org.JKDW.user.service;

import org.JKDW.user.model.Cuisines;

import java.util.List;

public interface CuisinesService {

	List<Cuisines> getAllCuisines();
	
}
