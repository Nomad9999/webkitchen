package org.JKDW.user.model.DTO;

public class StringRequestBody {

    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
